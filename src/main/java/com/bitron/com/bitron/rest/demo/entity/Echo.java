package com.bitron.com.bitron.rest.demo.entity;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Echo {
	private static final AtomicInteger counter = new AtomicInteger(0);
	private Integer id= counter.incrementAndGet();
	private String echo;
	private Date messageTime = new Date();
	
	
	
	public String getEcho() {
		return echo;
	}
	public void setEcho(String echo) {
		this.echo = echo;
	}
	public Date getMessageTime() {
		return messageTime;
	}
	public void setMessageTime(Date messageTime) {
		this.messageTime = messageTime;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public static AtomicInteger getCounter() {
		return counter;
	}
	@Override
	public String toString() {
		return "Echo [id=" + id + ", echo=" + echo + ", messageTime=" + messageTime + "]";
	}
	
	

}
