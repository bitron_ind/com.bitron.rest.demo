package com.bitron.com.bitron.rest.demo.rs;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bitron.com.bitron.rest.demo.entity.Echo;

@RestController
@RequestMapping("/echo")
public class EchoService {

	
    @GetMapping("/{message}")
    public Echo getById(@PathVariable("message") String message) {
        Echo echo = new Echo();
        echo.setEcho(message);
        return echo;
    }
	
	
}
